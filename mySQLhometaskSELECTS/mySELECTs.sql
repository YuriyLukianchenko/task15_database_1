use Labor_SQL;
-- my selectors
-- 1
SELECT maker, type FROM Product ORDER BY maker ASC;
-- 2
SELECT model, ram, screen, price FROM Laptop 
    WHERE price > 1000 ORDER BY ram ASC, price DESC;
-- 3
SELECT * FROM Printer WHERE color = 'y' 
    ORDER BY price DESC;
-- 4
SELECT model, speed, hd, cd, price FROM PC
    WHERE (cd = '12x' OR cd = '24x') AND price < 600
    ORDER BY speed DESC;
-- <ships>
-- 5
SELECT name, class FROM Ships ORDER BY name ASC;
-- </ships>
-- 6
SELECT * FROM PC
    WHERE speed >= 500 AND price < 800
    ORDER BY price DESC;
-- 7
SELECT * FROM Printer 
    WHERE type <> 'Matrix' AND price < 300
    ORDER BY price DESC;
-- 8
SELECT model, speed FROM PC
    WHERE price >= 400 AND price <= 600
    ORDER BY hd ASC;
-- 9
SELECT * FROM PC, Product -- bad SELECT !!!!
    WHERE (PC.hd = 10 OR PC.hd = 20) AND Product.maker = 'A' 
    ORDER BY PC.speed ASC;
-- 10
SELECT model, speed, hd, price FROM Laptop
    WHERE screen >= 12
    ORDER BY price DESC;
-- 11
SELECT model, type, price FROM Printer
    WHERE price < 300
    ORDER BY type DESC;
-- 12
SELECT model, ram, price FROM Laptop
    WHERE RAM = 64
    ORDER BY screen;
-- 13 
SELECT model, ram, price FROM PC
    WHERE ram > 64
    ORDER BY hd;
-- 14
SELECT model, speed, price FROM PC
    WHERE speed >= 500 AND speed <= 750
    ORDER BY hd DESC;
-- 15
SELECT * FROM Outcome_o O
    WHERE O.out > 2000
    ORDER BY date DESC;
-- 16
SELECT * FROM Income_o 
    WHERE inc >= 5000 AND inc <= 10000
    ORDER BY inc;
-- 17
SELECT * FROM Income
    WHERE point = 1
    ORDER BY inc;
-- 18
SELECT * FROM Outcome O
    WHERE point = 2
    ORDER BY O.out;
-- 19
SELECT * FROM Classes
    WHERE country = 'Japan'
    ORDER BY type DESC;
-- 20
SELECT name, launched FROM Ships
    WHERE launched >= 1920 AND launched <= 1942
    ORDER BY launched DESC;
-- 21
SELECT ship, battle, result FROM Outcomes
    WHERE battle = 'Guadalcanal' AND result <> 'sunk'
    ORDER BY ship DESC;
-- 22
SELECT ship, battle, result FROM Outcomes
    WHERE result = 'sunk'
    ORDER BY ship DESC;
-- 23
SELECT  class, displacement FROM Classes
    WHERE displacement >= 40000
    ORDER BY type;
-- 24
SELECT trip_no, town_from, town_to FROM Trip
    WHERE town_from = 'London' OR town_to = 'London'
    ORDER BY time_out;
-- 25
SELECT trip_no, plane, town_from, town_to FROM Trip
    WHERE plane = 'TU-134'
    ORDER BY time_out DESC;
-- 26
SELECT trip_no, plane, town_from, town_to FROM Trip
    WHERE plane != 'IL-86'
    ORDER BY plane;
-- 27
SELECT trip_no, town_from, town_to FROM Trip
    WHERE town_from != 'Rostov' AND town_to != 'Rostov'
    ORDER BY plane;
-- 1
SELECT model FROM PC
    WHERE model LIKE '%1%1%';
-- 2
SELECT * FROM Outcome
    WHERE date LIKE '%-03-%';
-- 3
SELECT * FROM Outcome_o
    WHERE date LIKE '%-14%';
-- 4
SELECT name FROM Ships
    WHERE name LIKE 'W%n';
-- 5 
SELECT name FROM Ships
    WHERE name LIKE '%e%';
-- 6
SELECT name, launched FROM Ships
    WHERE name NOT LIKE '%a';
-- 7
SELECT name FROM Battles
    WHERE name LIKE '% %' AND name  NOT LIKE '%c';
-- 8
SELECT * FROM Trip
    WHERE time_out RLIKE '....-..-.. 1[2-6]:' 
        OR time_out RLIKE '....-..-.. 17:00:00.000';
-- 9
SELECT * FROM Trip
   WHERE time_in RLIKE '....-..-.. 1[7-9]:'
      OR time_in RLIKE '....-..-.. 2[0-2]:' 
      OR time_in RLIKE '....-..-.. 23:00:00';
-- aditional select
SELECT * FROM Trip
    WHERE town_to = 'London';
-- 10 
SELECT date FROM Pass_in_trip
    WHERE place RLIKE '1';
-- 11 
SELECT date FROM Pass_in_trip
    WHERE place RLIKE 'c';
-- 12
SELECT substring_index(name,' ',-1) as surname  FROM Passenger
    WHERE name RLIKE ' C';
-- 13
SELECT substring_index(name,' ',-2) as surname FROM Passenger
    WHERE name NOT RLIKE ' J';   
-- 1
SELECT maker, type ,speed, hd 
    FROM PC  LEFT JOIN PRODUCT PR ON PC.model = PR.model
    WHERE hd <= 8;
-- 2
SELECT maker 
    FROM PC LEFT JOIN Product PR ON PC.model = PR.model
    WHERE speed >= 600;
-- 3
SELECT maker 
    FROM Laptop L LEFT JOIN Product P ON L.model = P.model
    WHERE speed <= 500;
-- 4 how print firstm model with higher number ?
SELECT DISTINCT L1.model model1, L2.model model2, L2.hd, L2.ram 
    FROM Laptop L1 JOIN Laptop L2
    WHERE (L1.hd = L2.hd) AND L1.ram = L2.ram AND L1.code <> L2.code ;
-- 5 dont understand task (  maybe класи з типом, а не типи з класом
SELECT * 
FROM Classes C1 JOIN Classes C2
WHERE C1.class <> C2.Class 
	AND (C1.type = 'bb' AND C2.type = 'bc') 
    AND C1.country = C2.country;
-- 6 
SELECT PC.model, maker
FROM PC Pc LEFT JOIN Product Pr ON Pc.model = Pr.model  
WHERE price < 600;
-- 7
SELECT Pr.model, maker
FROM Printer Pc LEFT JOIN Product Pr ON Pc.model = Pr.model  
WHERE price > 300;
-- 8
(SELECT maker, Pc.model, price 
FROM PC Pc LEFT JOIN Product Pr ON Pc.model = Pr.model)
UNION ALL
(SELECT maker, Lt.model, price 
FROM Laptop Lt LEFT JOIN Product Pr ON Lt.model = Pr.model);
-- 9 strange task
SELECT Pr.maker, Pr.model, Pc.price
FROM  PC Pc LEFT JOIN Product Pr ON Pr.model = Pc.model;
-- 10 
SELECT Pr.maker, Pr.type, Pr.model , Lt.speed
FROM Laptop Lt LEFT JOIN Product Pr ON Pr.model = Lt.model
WHERE speed > 600;
-- 11
SELECT Cl.displacement
FROM Ships Sh LEFT JOIN Classes Cl ON Sh.class = Cl.class;
-- 12
SELECT B.name, B.date
FROM Outcomes O LEFT JOIN Battles B ON O.battle = B.name
WHERE result <> 'sunk';
-- 13
SELECT C.country
FROM Ships S LEFT JOIN Classes C ON S.class = C.class;
-- 14
SELECT C.name
FROM Trip T LEFT JOIN Company C ON T.ID_comp = C.ID_comp
WHERE plane = 'Boeing'; 
-- 15
SELECT date, name
FROM Pass_in_trip P LEFT JOIN Passenger Ps ON Ps.ID_psg = P.ID_psg;
    